import React from "react";
import {
  Box,
  Heading,
  Pagination,
} from 'grommet';
import { Dish } from "./Dish";


export const Dishes = ({ dishes, page, totalNumberDishes, handlePageChange }) => <Box>
  <Heading level="2">List of potential dishes:</Heading>
  <Heading level="3">{`total ${totalNumberDishes} dishes found`}</Heading>
  {totalNumberDishes > 0 && <Pagination
    page={page}
    numberItems={totalNumberDishes}
    step={100}
    numberEdgePage={0}
    numberMiddlePages={0}
    onChange={handlePageChange}
  />}
  <Box direction="row" justify="around" gap="small" wrap>
    {dishes.map((dish) =>
      <Dish {...dish} key={dish.id} />
    )}
  </Box>
</Box>