import React, { useState } from 'react';
import {
  Box,
  Button,
  CheckBox,
  Grommet,
  grommet,
  Heading,
  Spinner,
  Text,
  TextInput
} from 'grommet';
import { Dishes } from "./Dishes";

function App() {

  const [searchText, setSearchText] = useState('');
  const [dishes, setDishes] = useState([]);
  const [strictMode, setStrictMode] = useState(true);
  const [pagyMeta, setPagyMeta] = useState({ count: 0, page: 1, last: 1 });
  const [isLoading, setIsLoading] = useState(false);

  const handleOnChange = event => setSearchText(event.target.value)

  const fetchDishes = async (page = 1) => {
    setIsLoading(true);
    setDishes([]);
    const response = await fetch(`https://pennylane-recipes-2021.herokuapp.com/api/dish/${searchText}?strictMode=${strictMode}&page=${page}`)

    if (response.ok) {
      const result = await response.json();
      const newDishes = result.data || [];
      setDishes(newDishes);
      setPagyMeta(result.pagy);

    }
    setIsLoading(false);
  }

  const handleOnClick = async () => {
    await fetchDishes();
  }

  const handlePageChange = async ({ page }) => {
    await fetchDishes(page);
  }


  return (
    <Grommet theme={grommet}>
      <Box margin="large" gap="medium" direction="column">
        <Heading level="1">Recipes</Heading>
        <Heading level="4" margin="none">Strict Mode</Heading>
        <CheckBox
          checked={strictMode}
          toggle
          label="All typed ingredients should be in the recipes, except excluded ones"
          onChange={event =>
            setStrictMode(event.target.checked)
          }
        />
        <TextInput
          placeholder="Type ingredients here"
          value={searchText}
          onChange={handleOnChange}
        />
        <Text weight="normal" color="grey">Want to exclude an ingredient?  ex: Jambon !oeuf</Text>

        <Button
          primary
          size="small"
          label="Send"
          onClick={handleOnClick}
          disabled={searchText.length === 0 || isLoading}
        />

        <Box>
          {isLoading && <Spinner />}
        </Box>
        <Dishes
          dishes={dishes}
          page={pagyMeta.page}
          totalNumberDishes={pagyMeta.count}
          handlePageChange={handlePageChange}
        />

      </Box>
    </Grommet>
  );
}

export default App;
