import { Box, Card, CardBody, CardFooter, CardHeader, Image, Text } from "grommet";
import React from "react";


const List = ({ dishId, ingredients }) =>
  <Box direction="column">
    {ingredients.map((ingredient, index) => <Text key={`${dishId} ${index} ${ingredient}`}>{ingredient}</Text>)}
  </Box>;

export const Dish = ({ id, ingredients, name, image, rate, tags }) => {

  const rateString = rate ? `(${rate || "0"}/5)` : "";

  return (
    <Box margin="1rem">
      <Card height="medium" width="medium" background="light-1">
        <CardHeader pad="medium"><Text weight="bold">{`${name} ${rateString}`}</Text> </CardHeader>
        <CardBody pad="medium" overflow="scroll">
          {image && <Image
            fit="contain"
            src={image}
          />}
          <List
            ingredients={ingredients}
            dishId={id}
          />
        </CardBody>
        <CardFooter pad={{ horizontal: "small" }} background="light-2">
          <Text>{`Tags: ${tags.join(', ')}`} </Text>
        </CardFooter>
      </Card>
    </Box>)
}
