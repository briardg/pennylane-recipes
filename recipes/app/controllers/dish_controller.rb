# frozen_string_literal: true

class DishController < ApplicationController

  def index
    dishes = Dish.keywords_search(!strict_mode, search_text)
    pagy, dishes = pagy(dishes)
    render json: {
      data: dishes,
      pagy: pagy_metadata(pagy)  
    }
  end
end

def search_text
  (params[:search_text] || "").strip
end

def strict_mode
  (params[:strictMode] || "").strip == "true"
end