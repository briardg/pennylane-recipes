class Dish < ApplicationRecord
  include PgSearch::Model

  serialize :ingredients, Array
  serialize :tags, Array

  pg_search_scope :keywords_search, lambda { |any_word = false, query| 
    {
      against: {
        ingredients: "A",
        tags: "B"
      },
      using: {
        tsearch: {
          any_word: any_word,
          prefix: true,
          normalization: 2,
          negation: true
        },
      },
      ignoring: :accents,
      query: query
    }
  }
end
