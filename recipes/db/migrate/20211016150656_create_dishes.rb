class CreateDishes < ActiveRecord::Migration[5.2]
  def change
    create_table :dishes do |t|
      t.integer :rate
      t.string :author_tip
      t.string :budget
      t.string :prep_time
      t.text :ingredients
      t.string :name
      t.string :author
      t.string :difficulty
      t.integer :people_quantity
      t.string :cook_time
      t.text :tags
      t.string :total_time
      t.string :image
      t.integer :nb_comments

      t.timestamps
    end
  end
end
